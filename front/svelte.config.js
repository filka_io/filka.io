import { isoImport } from 'vite-plugin-iso-import'

const mode = process.env.SVELTE_MODE;

const nginxDevConfig = {
  kit: {
    trailingSlash: 'always',
    vite: {
      plugins: [
        isoImport(),
      ],
      server: {
        port: 3000,
        strictPort: true,
        hmr: {
          port: 3000,
          protocol: "ws"
        }
      },
      resolve: {
        dedupe: ['@fullcalendar/common'],
        browser: true,
      },
      optimizeDeps: {
        include: ['@fullcalendar/common']
      }
    }
  },
};

const defaultConfig = {
  kit: {
    trailingSlash: 'always',
    vite: {
      plugins: [
        isoImport(),
      ],
      resolve: {
        dedupe: ['@fullcalendar/common'],
        browser: true,
      },
      optimizeDeps: {
        include: ['@fullcalendar/common']
      }
    }
  },
};

const config = (() => {
  switch (mode) {
    case "nginx_dev": return nginxDevConfig;
    default: return defaultConfig;
  }
})();

export default config;
