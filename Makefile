SHELL := /bin/bash

default:
	scripts/dev.sh

setup-dev:
	scripts/check-python.py
	rm -rf venv
	python3 -m venv venv || python3 -m virtualenv venv
	source venv/bin/activate && \
		pip install --upgrade pip \
		pip install -r requirements.txt
	cd front && yarn install
	scripts/venv-warning.sh

nginx-dev:
	SVELTE_MODE=nginx_dev make services -j2

services: flask svelte

flask:
	FLASK_ENV=development FLASK_APP=climco flask run --reload 

svelte:
	cd front && yarn dev
