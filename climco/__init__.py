from flask import Flask
from authlib.integrations.flask_client import OAuth
from climco.config import Config, load_parameter
from typing import Any
from rethinkdb import RethinkDB

## App

app = Flask(__name__)
app.config.from_object(Config)
app.secret_key = "TODO: Change this token"

## OAuth

cache = None

oauth = OAuth(app, cache=cache)
oauth.register(
    name="supla",
    access_token_url="https://svr64.supla.org/oauth/v2/token",
    access_token_params=None,
    authorize_url="https://svr64.supla.org/oauth/v2/auth",
    authorize_params=None,
    api_base_url="https://svr64.supla.org/",
    client_kwargs={"scope": "account_r channels_r"},
)

supla: Any = oauth.create_client("supla")

## Database

NEEDED_TABLES = ["users"]


def db():
    conn = RethinkDB()
    conn.connect(
        load_parameter("DATABASE_HOST"), load_parameter("DATABASE_PORT")
    ).repl()
    return conn


conn = db()
tables_in_db = conn.table_list().run()
for table in NEEDED_TABLES:
    if table not in tables_in_db:
        conn.table_create(table).run()

from climco import api
