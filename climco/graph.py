import numpy as np
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
import mplcursors
import pandas as pd
import mpld3


def printGraph(JSON):

    df = pd.DataFrame.from_records(data=JSON)
    df["time"] = pd.to_datetime(df["date_timestamp"], unit="s")
    df["temperature"] = pd.to_numeric(df["temperature"])
    df["humidity"] = pd.to_numeric(df["humidity"])
    df.set_index("time")

    fig, ax = plt.subplots()
    ax2 = ax.twinx()

    # no hover labels in this case
    # ax.plot(df['time'], df['temperature'], color='red')
    # ax2.plot(df['time'], df['humidity'])

    # fucked up labels in this case lmao
    df.temperature.plot(ax=ax, color="red", y="time")
    df.humidity.plot(ax=ax2, secondary_y=True)

    ax.set_xlabel(df.columns[3])
    ax.set_ylabel(df.columns[1])
    ax2.set_ylabel(df.columns[1])

    ax.xaxis.set_major_locator(DayLocator())
    ax.xaxis.set_minor_locator(HourLocator(interval=24))
    ax.xaxis.set_major_formatter(DateFormatter("%Y-%m-%d"))

    ax.fmt_xdata = DateFormatter("%Y-%m-%d %H:%M:%S")
    fig.autofmt_xdate()
    mplcursors.cursor(hover=True)
    # plt.show()
    return mpld3.fig_to_html(fig)
