#!/bin/sh

# DIRNAME=$(dirname "$0")
# if [ "$DIRNAME" != "." ]; then
#   echo "Plese run this script from root of the project"
#   exit
# fi

if [ "$VIRTUAL_ENV" = "" ]; then
  scripts/venv-warning.sh
  exit
fi

echo "Server will be launched on https://localhost/"

sleep 1

sudo -n true
WAIT_FOR_PASSWORD="$?"


if [ "$WAIT_FOR_PASSWORD" != "0" ]; then
  echo "You have 7 seconds to input sudo password"
fi

sudo docker-compose -f docker/docker-compose.yml up &

if [ "$WAIT_FOR_PASSWORD" != "0" ]; then
  sleep 7
fi

make nginx-dev

